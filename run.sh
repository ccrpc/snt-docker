# With the image created in the previous step, mount all the neccessary 
# directory as volume so the container has access to all the data

# /home/kml42638@co.champaign.il.us/data/:/mnt/data/
# Output location 

# /home/kml42638@co.champaign.il.us/git/:/mnt/git/
# This contains three repos, cuuats.snt.lts and cuuats.snt.accessibility, snt-processing
# This 3 repos has scripts and code that actually run the analysis
# You can mount different volume as long as you have access to these 3 repos

# /mnt/GIS/CUUATS/Sustainable\ Neighborhoods\ Toolkit/:/mnt/SNT/
# Output location

# Run `-it` in interactive mode using `snt` image with bash 

docker run --rm -v /home/kml42638@co.champaign.il.us/data/:/mnt/data/ \
  -v /home/kml42638@co.champaign.il.us/git/:/mnt/git/ \
  -v /mnt/GIS/CUUATS/Sustainable\ Neighborhoods\ Toolkit/:/mnt/SNT/ \
  -it snt bash
