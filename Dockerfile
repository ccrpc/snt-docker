FROM registry.gitlab.com/ccrpc/python-gis

# extra metadata
LABEL version="0.1"
LABEL description="Image containing Pandana, LTS and Accessibility libraries for run Sustainable Neighborhood"

ENV PYTHONUNBUFFERED 1
WORKDIR /build
ADD requirements.txt /build/
COPY snt /usr/local/bin/snt
# COPY cuuats.snt.lts/ /build/cuuats.snt.lts/

RUN chmod +x /usr/local/bin/snt

RUN apt update \
  && apt install -y \
  gcc \
  python3-dev \
  build-essential \
  libgomp1 \
  && pip3 install --no-cache-dir -r requirements.txt \
  && apt remove -y gcc \
  python3-dev \
  build-essential \
  && rm -r /var/lib/apt/lists/* \
  && apt -qy autoremove
